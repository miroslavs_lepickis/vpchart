const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
module.exports = {
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js'
    },
    plugins: [
        new CopyWebpackPlugin([
            {
                from: './node_modules/@dvsl/zoomcharts/lib/assets',
                to: 'assets'
            },
            {
                from: './node_modules/bootstrap/dist',
                to: 'bootstrap'
            },
            {
                from: './node_modules/font-awesome/css',
                to: 'font-awesome/css'
            },
            {
                from: './resources/assets/img',
                to: 'img'
            },
            {
                from: './node_modules/jquery/dist',
                to: 'jquery'
            }
        ])
    ]
};