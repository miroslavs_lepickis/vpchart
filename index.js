import * as zc from "@dvsl/zoomcharts"

let NetChart = zc.NetChart;

let connectNodeIsActive = false;
let selectedNode = null;
let selectedLink = null;
let customNodeOrLinkId = 0;
let nodeOrLinkInEditor = null;
let searchAllowed = false;

let t = new NetChart({
    container: document.getElementById("chart"),
    area: {
        height: 650,
    },
    auras: {
        cellSize: 10,
        overlap: true,
        enabled: true,
        defaultStyle: {
            showInLegend: true,
            shadowBlur: 35
        },
        style: {
            'case-1': {
                fillColor: "rgba(254,248,17,0.3)"
            },
            'case-2': {
                fillColor: "rgba(1, 198, 181,0.3)"
            }
        }
    },
    data: {
        url: '/api/chart/data'
    },
    style: {
        nodeStyleFunction: nodeStyle,
        linkStyleFunction: linkStyle,
    },
    legend: {
        enabled: true,
        padding: 6,
        marker: {size: 22},
        maxLineSymbols: 12,
    },
    events: {
        onClick: function (event, args) {
            if (args.clickNode) {
                if (connectNodeIsActive) {
                    this.addData({links:[{id: 'cl' + customNodeOrLinkId, from: selectedNode.data.id, to: args.clickNode.data.id}]});
                    customNodeOrLinkId++;

                    selectedNode = null;
                    connectNodeIsActive = false;
                } else {
                    selectedNode = args.clickNode;
                }
            } else {
                selectedNode = null;
                connectNodeIsActive = false;
                this.updateStyle();
            }

            selectedLink = (args.clickLink) ? args.clickLink : null;

            this.updateSettings({
                toolbar: {extraItems: toolbarExtraItems(selectedNode, selectedLink)}
            });
        },
        onDoubleClick: function (event, args) {
            if (args.clickNode || args.clickLink) {
                editNodeOrLink((args.clickNode) ? args.clickNode : args.clickLink);
            }

            event.preventDefault();
        },
    },
    toolbar: {
        export: true,
        fullscreen: false,
        extraItems: toolbarExtraItems(null),
    },
    advanced: {
        crossOriginHeader: 'anonymous',
    },
    interaction: {selection: {lockNodesOnMove: true}},
});

function nodeStyle(node) {

    node.display = 'roundtext';

    node.aura = node.data.auras;
    node.label = node.data.name;
    node.items = [];

    // Add icons to classIdentities, classIpAddresses, classOtherInformation
    if (typeof node.data.className !== 'undefined' &&
        (node.data.className === 'classIdentities' || node.data.className === 'classIpAddresses' || node.data.className === 'classOtherInformation' || node.data.className === 'classUploads')
    ) {
        node.display = 'image';
        node.label = '';
        node.radius = 40;

        node.image = 'img/charts/' + node.data.className + '.svg';
    }

    if (typeof node.data.className !== 'undefined' && node.data.className === 'hidden') {
        t.removeData({
            nodes: [node]
        });
        return;
    }

    // Add style to classCase
    if (typeof node.data.className !== 'undefined' && node.data.className === 'classCase') {
        node.labelStyle.textStyle.fillColor = 'white';
        node.fillColor = "rgb(5,39,68)";
        node.radius = 60;
    }

    // Add subtitles to nodes with extra.subtitle
    if (typeof node.data.extra !== 'undefined' && (typeof node.data.extra.subtitle !== 'undefined' && node.data.extra.subtitle !== '')) {
        node.items = [
            {
                text: node.data.extra.subtitle,
                px: 0, py: 1, x: 0, y: 0,
            },
        ];
    }

    // Add connect to another node message
    if (selectedNode !== null && connectNodeIsActive) {
        selectedNode.items.push({
            text: 'Select a node to connect',
            px: 0, py: -1, x: 0, y: 0,
            textStyle: {fillColor: "white"},
            backgroundStyle: {
                fillColor: '#ac0b11',
                lineColor: 'transparent',
            }
        });
    }

    // Add style to search results
    if (typeof node.data.extra !== 'undefined' && node.data.extra.searchResult && searchAllowed) {
        node.lineColor = '#6d0103';
        node.lineWidth = 3;
    }
}

function linkStyle(link) {
    link.label = link.data.label;
}

function toolbarExtraItems(clickNode = null, clickLink = null) {
    return [
        // Connect nodes button
        {
            enabled: !!clickNode,
            image: "img/charts/connect-node.png",
            title: 'Connect the node',
            onClick: function () {
                connectNodeIsActive = true;
                t.updateStyle();
            },
        },
        // Add nodes button
        {
            image: "img/charts/add-node.png",
            title: 'Add a new node',
            dropDownItems: [
                {
                    'label': 'Add Ip Addresses',
                    onClick: function() {
                        t.addData({nodes:[{id: 'cn-' + customNodeOrLinkId, name: 'Ip addresses', className: 'classIpAddresses'}]});
                        customNodeOrLinkId++;

                        t.updateSettings({
                            toolbar: {extraItems: toolbarExtraItems()}
                        });
                    }
                },
                {
                    'label': 'Add Other Info',
                    onClick: function() {
                        t.addData({nodes:[{id: 'cn-' + customNodeOrLinkId, name: 'Other information', className: 'classOtherInformation'}]});
                        customNodeOrLinkId++;

                        t.updateSettings({
                            toolbar: {extraItems: toolbarExtraItems()}
                        });
                    }
                },
                {
                    'label': 'Add Identity',
                    onClick: function() {
                        t.addData({nodes:[{id: 'cn-' + customNodeOrLinkId, name: 'Identities', className: 'classIdentities'}]});
                        customNodeOrLinkId++;

                        t.updateSettings({
                            toolbar: {extraItems: toolbarExtraItems()}
                        });
                    }
                },
                {
                    'label': 'Add Data',
                    onClick: function() {
                        t.addData({nodes:[{id: 'cn-' + customNodeOrLinkId, name: 'Data'}]});
                        customNodeOrLinkId++;

                        t.updateSettings({
                            toolbar: {extraItems: toolbarExtraItems()}
                        });
                    }
                },
            ]
        },
        // Delete nodes or links button
        {
            enabled: !!clickNode || !!clickLink,
            image: "img/charts/delete-node.png",
            title: 'Delete the node',
            onClick: function () {
                connectNodeIsActive = false;

                if (selectedNode) {
                    t.removeData({
                        nodes: [clickNode]
                    });
                    selectedNode = null;
                }
                if (selectedLink) {
                    t.removeData({
                        links: [clickLink]
                    });

                    selectedLink = null;
                }

                t.updateSettings({
                    toolbar: {extraItems: toolbarExtraItems()}
                });
            },
        },
        // Save button TODO: Add BE
        {
            image: "img/charts/save.png",
            title: 'Save and exit',
            onClick: function () {
                var exportedData = t.exportData();
                console.log(JSON.stringify(exportedData));
            },
        },
    ]
}

function editNodeOrLink(clickNodeOrLink) {
    nodeOrLinkInEditor = clickNodeOrLink;

    $('#popup-editor .node-editor, #popup-editor .link-editor').hide();
    $('#popup-editor').modal();

    if (clickNodeOrLink.isNode) {
        let clickNode = clickNodeOrLink;
        $('#popup-editor .node-editor').show();
        $('#popup-editor #name').val(clickNode.data.name);
        $('#popup-editor #subtitle').val((clickNode.data.extra && clickNode.data.extra.subtitle) ? clickNode.data.extra.subtitle : '');
        $('#popup-editor #auras').val((clickNode.data.auras) ? clickNode.data.auras : '');
    } else {
        let clickLink = clickNodeOrLink;
        $('#popup-editor .link-editor').show();
        $('#popup-editor #label').val((clickLink.data.label) ? clickLink.data.label : '');
    }
}

function saveNodeOrLink() {
    $('#popup-editor').modal('hide');
    if (nodeOrLinkInEditor) {
        if (nodeOrLinkInEditor.isNode) {
            let node = nodeOrLinkInEditor;
            node.data.name = $('#popup-editor #name').val();
            if (typeof node.data.extra === 'undefined' ) {
                node.data.extra = {};
            }
            node.data.extra.subtitle = $('#popup-editor #subtitle').val();
            node.data.auras = $('#popup-editor #auras').val();
        } else {
            let link = nodeOrLinkInEditor;
            link.data.label = $('#popup-editor #label').val();
        }

        t.updateStyle();
    }
}

function searchNode() {
    var data = t.nodes();
    var searchValue = $('#popup-search #search').val().toLowerCase();

    if (searchValue.length >= 3) {
        searchAllowed = true;
        data.forEach(function(item) {
            if (typeof item.data.extra === 'undefined' ) {
                item.data.extra = {};
            }
            item.data.extra.searchResult = item.data.name.toLowerCase().includes(searchValue);
        });
    } else {
        searchAllowed = false;
    }
    t.updateStyle();
}