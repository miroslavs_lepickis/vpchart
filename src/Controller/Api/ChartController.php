<?php

namespace App\Controller\Api;

use App\Service\ChartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChartController extends AbstractController
{
    private ChartService $chartService;

    public function __construct(ChartService $chartService)
    {
        $this->chartService = $chartService;
    }

    /**
     * @Route("/api/chart/data", name="api.chart.data")
     */
    public function data(): Response
    {
        // Structured data
        $data = $this->chartService->getNetChartData();

        // Structured data with rules applied
        $dataWithRulesApplied = $this->chartService->applyNetChartDataRules($data);

        // Structured data with rules and subtitles applied
        $dataWithSubtitleApplied = $this->chartService->applyNetChartDataSubtitles($dataWithRulesApplied);

        return $this->json($dataWithSubtitleApplied);
    }
}