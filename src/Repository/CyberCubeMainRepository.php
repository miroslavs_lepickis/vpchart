<?php

namespace App\Repository;

use App\Entity\CyberCubeMain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CyberCubeMain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CyberCubeMainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CyberCubeMain::class);
    }

    public function getFirstTen(): Collection
    {
        return new ArrayCollection($this->findBy([], null, 10));
    }
}