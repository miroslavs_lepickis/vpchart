<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CyberCubeMain
 *
 * @ORM\Table(name="cyber_cube_main")
 * @ORM\Entity
 */
class CyberCubeMain
{
    public const TABLE_NAME = 'cyber_cube_main';

    /**
     * @var int
     *
     * @ORM\Column(name="ID_nr", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idNr;

    /**
     * @var string
     *
     * @ORM\Column(name="recieving_type", type="string", length=255, nullable=false)
     */
    private $recievingType;

    /**
     * @var string
     *
     * @ORM\Column(name="recieving_country", type="string", length=255, nullable=false)
     */
    private $recievingCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="recieving_date", type="string", length=255, nullable=false)
     */
    private $recievingDate;

    /**
     * @var string
     *
     * @ORM\Column(name="recieving_nr", type="string", length=255, nullable=false)
     */
    private $recievingNr;

    /**
     * @var string
     *
     * @ORM\Column(name="crime_type", type="string", length=255, nullable=false)
     */
    private $crimeType;

    /**
     * @var string
     *
     * @ORM\Column(name="damage", type="string", length=255, nullable=false)
     */
    private $damage;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", length=0, nullable=false)
     */
    private $notes;

    /**
     * @var string
     *
     * @ORM\Column(name="writer_ID", type="string", length=200, nullable=false)
     */
    private $writerId;

    /**
     * @var string
     *
     * @ORM\Column(name="writer_date_time", type="string", length=200, nullable=false)
     */
    private $writerDateTime;

    /**
     * @ORM\OneToMany(targetEntity="CyberCubeIdentity", mappedBy="idNrMain")
     */
    private $identities;

    /**
     * @ORM\OneToMany(targetEntity="CyberCubeLocation", mappedBy="idNrMain")
     */
    private $locations;

    /**
     * @ORM\OneToMany(targetEntity="CyberCubePhone", mappedBy="idNrMain")
     */
    private $phones;

    /**
     * @ORM\OneToMany(targetEntity="CyberCubeEmail", mappedBy="idNrMain")
     */
    private $emails;

    /**
     * @ORM\OneToMany(targetEntity="CyberCubeWeb", mappedBy="idNrMain")
     */
    private $webAddresses;

    /**
     * @ORM\OneToMany(targetEntity="CyberCubeWallet", mappedBy="idNrMain")
     */
    private $wallets;

    /**
     * @ORM\OneToMany(targetEntity="CyberCubeIp", mappedBy="idNrMain")
     */
    private $ipAddresses;

    /**
     * @ORM\OneToMany(targetEntity="UploadData", mappedBy="cyberCube")
     */
    private $uploads;

    public function getIdNr(): ?int
    {
        return $this->idNr;
    }

    public function getRecievingType(): ?string
    {
        return $this->recievingType;
    }

    public function getRecievingCountry(): ?string
    {
        return $this->recievingCountry;
    }

    public function getRecievingDate(): ?string
    {
        return $this->recievingDate;
    }

    public function getRecievingNr(): ?string
    {
        return $this->recievingNr;
    }

    public function getCrimeType(): ?string
    {
        return $this->crimeType;
    }

    public function getDamage(): ?string
    {
        return $this->damage;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function getWriterId(): ?string
    {
        return $this->writerId;
    }

    public function getWriterDateTime(): ?string
    {
        return $this->writerDateTime;
    }

    /**
     * @return Collection|CyberCubeIdentity[]
     */
    public function getIdentities(): Collection
    {
        return $this->identities;
    }

    /**
     * @return Collection|CyberCubeLocation[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    /**
     * @return Collection|CyberCubePhone[]
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    /**
     * @return Collection|CyberCubeEmail[]
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    /**
     * @return Collection|CyberCubeWeb[]
     */
    public function getWebAddresses(): Collection
    {
        return $this->webAddresses;
    }

    /**
     * @return Collection|CyberCubeWallet[]
     */
    public function getWallets(): Collection
    {
        return $this->wallets;
    }

    /**
     * @return Collection|CyberCubeIp[]
     */
    public function getIpAddresses(): Collection
    {
        return $this->ipAddresses;
    }

    /**
     * @return Collection|UploadData[]
     */
    public function getUploads(): Collection
    {
        return $this->uploads;
    }
}
