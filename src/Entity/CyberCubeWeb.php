<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CyberCubeWeb
 *
 * @ORM\Table(name="cyber_cube_web", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeWeb
{
    public const TABLE_NAME = 'cyber_cube_web';

    /**
     * @var int
     *
     * @ORM\Column(name="ID_web", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idWeb;

    /**
     * @var string
     *
     * @ORM\Column(name="web_address", type="string", length=255, nullable=false)
     */
    private $webAddress;

    /**
     * @var CyberCubeMain
     *
     * @ORM\ManyToOne(targetEntity="CyberCubeMain", inversedBy="webAddresses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_nr_main", referencedColumnName="ID_nr")
     * })
     */
    private $idNrMain;

    public function getIdWeb(): ?int
    {
        return $this->idWeb;
    }

    public function hasWebAddress(): bool
    {
        return (bool)$this->webAddress;
    }

    public function getWebAddress(): ?string
    {
        return $this->webAddress;
    }

    public function getIdNrMain(): ?CyberCubeMain
    {
        return $this->idNrMain;
    }
}
