<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CyberCubeIp
 *
 * @ORM\Table(name="cyber_cube_ip", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeIp
{
    public const TABLE_NAME = 'cyber_cube_ip';

    /**
     * @var int
     *
     * @ORM\Column(name="ID_ip", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idIp;

    /**
     * @var string
     *
     * @ORM\Column(name="IP_address", type="string", length=255, nullable=false)
     */
    private $ipAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="IP_date", type="string", length=255, nullable=false)
     */
    private $ipDate;

    /**
     * @var string
     *
     * @ORM\Column(name="IP_time", type="string", length=255, nullable=false)
     */
    private $ipTime;

    /**
     * @var CyberCubeMain
     *
     * @ORM\ManyToOne(targetEntity="CyberCubeMain", inversedBy="ipAddresses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_nr_main", referencedColumnName="ID_nr")
     * })
     */
    private $idNrMain;

    public function getIdIp(): ?int
    {
        return $this->idIp;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function getIpDate(): ?string
    {
        return $this->ipDate;
    }

    public function getIpTime(): ?string
    {
        return $this->ipTime;
    }

    public function getIdNrMain(): ?CyberCubeMain
    {
        return $this->idNrMain;
    }
}
