<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CyberCubeEmail
 *
 * @ORM\Table(name="cyber_cube_email", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeEmail
{
    public const TABLE_NAME = 'cyber_cube_email';

    /**
     * @var int
     *
     * @ORM\Column(name="ID_email", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="email_address", type="string", length=255, nullable=false)
     */
    private $emailAddress;

    /**
     * @var CyberCubeMain
     *
     * @ORM\ManyToOne(targetEntity="CyberCubeMain", inversedBy="emails")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_nr_main", referencedColumnName="ID_nr")
     * })
     */
    private $idNrMain;

    public function getIdEmail(): ?int
    {
        return $this->idEmail;
    }

    public function hasEmailAddress(): bool
    {
        return (bool)$this->emailAddress;
    }

    public function getEmailAddress(): ?string
    {
        return $this->emailAddress;
    }

    public function getIdNrMain(): ?CyberCubeMain
    {
        return $this->idNrMain;
    }
}
