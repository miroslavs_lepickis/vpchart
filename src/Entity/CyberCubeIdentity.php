<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CyberCubeIdentity
 *
 * @ORM\Table(name="cyber_cube_identity", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeIdentity
{
    public const TABLE_NAME = 'cyber_cube_identity';

    /**
     * @var int
     *
     * @ORM\Column(name="ID_identity", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idIdentity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="birth_date", type="string", length=255, nullable=false)
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="register_ID", type="string", length=255, nullable=false)
     */
    private $registerId;

    /**
     * @var string
     *
     * @ORM\Column(name="victim", type="string", length=255, nullable=false)
     */
    private $victim;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=255, nullable=false)
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="email_address", type="string", length=255, nullable=false)
     */
    private $emailAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="web_page", type="string", length=255, nullable=false)
     */
    private $webPage;

    /**
     * @var string
     *
     * @ORM\Column(name="wallet_id", type="string", length=255, nullable=false)
     */
    private $walletId;

    /**
     * @var CyberCubeMain
     *
     * @ORM\ManyToOne(targetEntity="CyberCubeMain", inversedBy="identities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_nr_main", referencedColumnName="ID_nr")
     * })
     */
    private $idNrMain;

    public function getIdIdentity(): ?int
    {
        return $this->idIdentity;
    }

    public function hasName(): bool
    {
        return (bool) $this->name;
    }

    public function getName(): string
    {
        return $this->name ?? 'UNKNOWN';
    }

    public function getBirthDate(): ?string
    {
        return $this->birthDate;
    }

    public function getRegisterId(): ?string
    {
        return $this->registerId;
    }

    public function getVictim(): ?string
    {
        return $this->victim;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function getEmailAddress(): ?string
    {
        return $this->emailAddress;
    }

    public function getWebPage(): ?string
    {
        return $this->webPage;
    }

    public function getWalletId(): ?string
    {
        return $this->walletId;
    }

    public function getIdNrMain(): ?CyberCubeMain
    {
        return $this->idNrMain;
    }
}
