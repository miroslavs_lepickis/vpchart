<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CyberCubeWallet
 *
 * @ORM\Table(name="cyber_cube_wallet", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeWallet
{
    public const TABLE_NAME = 'cyber_cube_wallet';

    /**
     * @var int
     *
     * @ORM\Column(name="ID_wallet", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idWallet;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_ID", type="string", length=255, nullable=false)
     */
    private $paymentId;

    /**
     * @var CyberCubeMain
     *
     * @ORM\ManyToOne(targetEntity="CyberCubeMain", inversedBy="wallets")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_nr_main", referencedColumnName="ID_nr")
     * })
     */
    private $idNrMain;

    public function getIdWallet(): ?int
    {
        return $this->idWallet;
    }

    public function hasPaymentId(): bool
    {
        return (bool)$this->paymentId;
    }

    public function getPaymentId(): ?string
    {
        return $this->paymentId;
    }

    public function getIdNrMain(): ?CyberCubeMain
    {
        return $this->idNrMain;
    }
}
