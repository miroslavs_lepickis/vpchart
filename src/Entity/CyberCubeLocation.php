<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CyberCubeLocation
 *
 * @ORM\Table(name="cyber_cube_location", indexes={@ORM\Index(name="ID_nr_main", columns={"ID_nr_main"})})
 * @ORM\Entity
 */
class CyberCubeLocation
{
    public const TABLE_NAME = 'cyber_cube_location';

    /**
     * @var int
     *
     * @ORM\Column(name="ID_location", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLocation;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=false)
     */
    private $location;

    /**
     * @var CyberCubeMain
     *
     * @ORM\ManyToOne(targetEntity="CyberCubeMain", inversedBy="locations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_nr_main", referencedColumnName="ID_nr")
     * })
     */
    private $idNrMain;

    public function getIdLocation(): ?int
    {
        return $this->idLocation;
    }

    public function hasLocation(): bool
    {
        return (bool) $this->location;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function getIdNrMain(): ?CyberCubeMain
    {
        return $this->idNrMain;
    }
}
