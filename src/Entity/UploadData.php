<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UploadData
 *
 * @ORM\Table(name="upload_data", indexes={@ORM\Index(name="cyber_cube_ID", columns={"cyber_cube_ID"})})
 * @ORM\Entity
 */
class UploadData
{
    public const TABLE_NAME = 'upload_data';

    /**
     * @var int
     *
     * @ORM\Column(name="upload_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uploadId;

    /**
     * @var string
     *
     * @ORM\Column(name="upload_record", type="string", length=255, nullable=false)
     */
    private $uploadRecord;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=50, nullable=false)
     */
    private $fileName;

    /**
     * @var CyberCubeMain
     *
     * @ORM\ManyToOne(targetEntity="CyberCubeMain", inversedBy="uploads")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cyber_cube_ID", referencedColumnName="ID_nr")
     * })
     */
    private $cyberCube;

    public function getUploadId(): ?int
    {
        return $this->uploadId;
    }

    public function hasUploadRecord(): bool
    {
        return (bool)$this->uploadRecord;
    }

    public function getUploadRecord(): ?string
    {
        return $this->uploadRecord;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function getCyberCube(): ?CyberCubeMain
    {
        return $this->cyberCube;
    }
}
