#### VP Zoomcharts

##### Installation

- Database connection setup
```
// .env
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
```

- Composer

```
composer install
```

- Npm

Project requires nodejs
```
npm install
```

Build assets
```
npm run start
```

Sample example
```
./index.js
```

- Setup zoomcharts keys

Please update your credentials in ./templates/chart.html.twig
```
var ZoomChartsLicense = "";
var ZoomChartsLicenseKey = "";
```

- Define NetChart custom rules

Please update ./config/services.yaml
```
netChartRules:
    -
        - 'identity.name'
        - 'upload.upload_record'
    -
        - 'identity.phone_number'
        - 'phone.phone_number'
        - 'upload.upload_record'
```

##### Api

```
GET /api/chart/data
```